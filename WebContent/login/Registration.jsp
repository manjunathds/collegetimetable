<%@ page language="java" 
		 contentType="text/html; charset=ISO-8859-1"
    	 pageEncoding="ISO-8859-1"
%>
<%@	page import="java.sql.*,
				 java.lang.*,
				 databaseconnection.*,
				 java.text.SimpleDateFormat,
				 java.util.*,
				 java.io.*,
				 javax.servlet.*,
				 javax.servlet.http.*"
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<%
String emailId  = request.getParameter("email");
String password = request.getParameter("pass");
String type		= request.getParameter("type");
String regno 	= request.getParameter("regno");
String lecId	= request.getParameter("lecId");
String sem	 	= request.getParameter("sem");
String fname 	= request.getParameter("fname");
String lname 	= request.getParameter("lname");
String phone 	= request.getParameter("phone");

Statement st = null;
PreparedStatement pst = null;
try {
	Connection con = databasecon.getconnection();
	pst = con.prepareStatement("insert into user_account values(?,?,?,?,?,?,?,?,?)");
	pst.setString(1,null);
	pst.setString(2,emailId);
	pst.setString(3,password);
	pst.setString(4,fname);
	pst.setString(5,lname);
	if(type.equals("Student")){
		pst.setString(6,regno);
		pst.setString(7,"S");
		pst.setString(8,sem);
	}
	else{
		pst.setString(6,lecId);
		pst.setString(7,"L");
		pst.setInt(8,0);
	}
	
	pst.setString(9,"I");
	pst.executeUpdate();
	pst.close();
	con.close();
	response.sendRedirect("login.jsp");
} catch (Exception ex) {
	ex.printStackTrace();
	out.println(ex);
}
%>
</body>
</html>