<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" href="../css/reg.css" media="screen" type="text/css" />
</head>
<body>
<form id="msform" action="Registration.jsp">
	<!-- progressbar -->
	<ul id="progressbar">
		<li class="active">Account Setup</li>
		<li>Type Of User</li>
		<li>Personal Details</li>
	</ul>
	<!-- fieldsets -->
	<fieldset>
		<h2 class="fs-title">Create your account</h2>
		<!--<h3 class="fs-subtitle">This is step 1</h3>!-->
		<input type="text" name="email" placeholder="Email" />
		<input type="password" name="pass" placeholder="Password" />
		<input type="password" name="cpass" placeholder="Confirm Password" />
		<input type="button" name="next" class="next action-button" value="Next" />
	</fieldset>
	<fieldset>
		<h2 class="fs-title">Type Of User</h2>
		<!--<h3 class="fs-subtitle">Your presence on the social network</h3>-->
		<!--<input type="text" name="twitter" placeholder="Twitter" />-->
		<select id="type" name="type"><option>Student</option><option>Lecturer</option></select>
		<input type="text" id="regno" name="regno" placeholder="Register No" />
		<input type="text" id="lecid" name="lecid" placeholder="Lecturer Id" />
		<input type="text" id="sem"   name="sem" placeholder="Sem" />
		<input type="button" name="previous" class="previous action-button" value="Previous" />
		<input type="button" name="next" class="next action-button" value="Next" />
	</fieldset>
	<fieldset>
		<h2 class="fs-title">Personal Details</h2>
		<!--<h3 class="fs-subtitle">We will never sell it</h3>-->
		<input type="text" name="fname" placeholder="First Name" />
		<input type="text" name="lname" placeholder="Last Name" />
		<input type="text" name="phone" placeholder="Phone" />
		<!--<textarea name="address" placeholder="Address"></textarea>-->
		<input type="button" name="previous" class="previous action-button" value="Previous" />
		<input type="submit" name="submit" class="submit action-button" value="Submit" />
	</fieldset>
</form>
<!-- jQuery -->
<script src="../js/jquery-1.9.1.min.js" type="text/javascript"></script>
<!-- jQuery easing plugin -->
<script src="../js/jquery.easing.min.js" type="text/javascript"></script>
<script src="../js/reg.js" type="text/javascript"></script>
</body>
</html>