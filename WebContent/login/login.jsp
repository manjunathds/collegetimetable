<%@ page language="java" 
		 contentType="text/html; charset=ISO-8859-1"
    	 pageEncoding="ISO-8859-1"
%>
	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Login Page</title>
<link rel="stylesheet" href="../css/style.css" media="screen" type="text/css" />
</head>
<body>
<div id="wrap">
  <div id="regbar">
    <div id="navthing">
      <h2><a href="#" id="loginform">Login</a> | <a id="regForm" href="Register.jsp">Register</a></h2>
    <form name="loginForm" action="logincheck.jsp">
	    <div class="login">
	      <div class="arrow-up"></div>
	      <div class="formholder">
	        <div class="randompad">
	           <fieldset>
	             <label>Email</label>
	             <input type="email" name="email" value="example@example.com" />
	             <label>Password</label>
	             <input name="password" type="password" />
	             <input  type="submit" value="Login" />
	           </fieldset>
	        </div>
	      </div>
    </div>
    </form>
    </div>
  </div>
</div>

  <script src='../js/jquery-1.9.1.min.js'></script>

  <script src="../js/index.js"></script>
</body>
</html>