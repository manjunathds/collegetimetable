<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>TimeTable</title>
<link href="../js/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href='../js/fullcalendar/fullcalendar.css' rel='stylesheet' />
<link href='../js/fullcalendar/fullcalendar.print.css' rel='stylesheet' media='print' />
<link href="../css/sweet-alert.css" rel="stylesheet">
<script src="../js/jquery-1.9.1.min.js"></script>
<script src="../js/bootstrap/js/bootstrap.min.js"></script>
<script src='../js/fullcalendar/lib/moment.min.js'></script>
<!-- <script src='../lib/jquery.min.js'></script> -->
<script src='../js/fullcalendar/fullcalendar.min.js'></script>
<script src="../js/sweet-alert.js"></script>
<script src="../js/underscore-min.js"></script>
<script src="../js/qtip/jquery.qtip-1.0.0-rc3.min.js"></script>

<script>

	$(document).ready(function() {
		var currentTimezone = false;
		var userData=null,url=null,sem=null,fullName=null;
		// load the list of available timezones
		/* $.getJSON('php/get-timezones.php', function(timezones) {
			$.each(timezones, function(i, timezone) {
				if (timezone != 'UTC') { // UTC is already in the list
					$('#timezone-selector').append(
						$("<option/>").text(timezone).attr('value', timezone)
					);
				}
			});
		}); */
		/* $.ajaxSetup({
			async:false
		});
		var days = [];
		$.get( "getCourseInfo.jsp",{sem : 6},doUpdate); */
		/* $.ajax({
	        url : 'getCourseInfo.jsp',
	        dataType: 'json',
	        success : doUpdate
	    }); */
		/* function doUpdate(response)
		{
		  if (response) {
		      $("#result").val(response);
		      console.log(response);
		      days =  _.groupBy(response,"day");
		      renderCalendar(days);
		  }
		} */
		var days = [];
		//renderCalendar(days);
		// when the timezone selector changes, rerender the calendar
		$.ajaxSetup({async:false});
		$.get("logedInfo.jsp",function(data){
			userData = data[0];
			if(userData.type == "L"){
				url = "getLecturerInfo.jsp";
				sem = userData.sem;
				fullName = userData.firstName+" "+userData.lastName;
			}
			else if(userData.type == "S"){
				url = "getCourseInfo.jsp";
				sem = userData.sem;
				fullName="";
			}
		});
		
		
		renderCalendar();
		$('#timezone-selector').on('change', function() {
			currentTimezone = this.value || false;
			$('#calendar').fullCalendar('destroy');
			renderCalendar(days);
		});

		function renderCalendar() {
			$('#calendar').fullCalendar({
				header: {
					left: 'prev,next today',
					center: 'title',
					right: 'agendaWeek,agendaDay'
				},
				defaultDate: moment().valueOf(),
				defaultView: 'agendaDay',
				allDaySlot:false,
				slotDuration:"00:15:00",
				minTime:"10:00:00",
				maxTime:"17:00:00",
				scrollTime:"08:00:00",
				timezone: currentTimezone,
				editable: userData.type=="L"?true:false,
				eventLimit: true, // allow "more" link when too many events
				/* events: function(start, end, timezone, callback) {
			        $.ajax({
			            url: 'getCourseInfo.jsp',
			            dataType: 'json',
			            data: {
			                sem : 6
			            },
			            success: function(doc) {
			                var events = [],days = [];
			                days = _.groupBy(doc,"day");
			                var day = moment().get("day");
			                var dayArray = [];
			                day == 1?dayArray = days.Monday:(day == 2?dayArray = days.Tuesday:(day == 3?dayArray = days.Wednesday:(day == 4?dayArray = days.Thursday:(day == 5?dayArray = days.Friday:(day == 6?dayArray = days.Saturday:([]))))));
			                $.each(dayArray,function(i,field) {
			                    events.push({
			                        title: field.cName,
			                        start: moment().hour(field.from.substring(0,2) == "10"?(10):(field.from.substring(0,2) == "11"?(11):(field.from.substring(0,2) == "12"?(12):(field.from.substring(0,1) == "01"?(13):(field.from.substring(0,2) == "02"?(14):(field.from.substring(0,2) == "03"?(15):(field.from.substring(0,2) == "04"?(16):(field.from.substring(0,2) == "05"?(17):(00))))))))).minute(00).second(00),
			                    	end:moment().hour(field.to.substring(0,2) == "10"?(10):(field.to.substring(0,2) == "11"?(11):(field.to.substring(0,2) == "12"?(12):(field.to.substring(0,2) == "01"?(13):(field.to.substring(0,2) == "02"?(14):(field.to.substring(0,2) == "03"?(15):(field.to.substring(0,2) == "04"?(16):(field.to.substring(0,2) == "05"?(17):(00))))))))).minute(00).second(00)// will be parsed
			                    });
			                }); 
			                events.push({
		                        title: "Lunch Break",
		                        start: moment().hour(13).minute(00).second(00),
		                    	end:moment().hour(14).minute(00).second(00)// will be parsed
		                    });
			                //events = _.groupBy(doc,"day");
			                callback(events);
			            }
			        });
			    }, */
				loading: function(bool) {
					$('#loading').toggle(bool);
				},
				eventRender: function(event, el) {
					// render the timezone offset below the event title
					/* if (event.start.hasZone()) {
						el.find('.fc-title').after(
							$('<div class="tzo"/>').text(event.start.format('Z'))
						);
					} */
					//console.log(event.description);
					/* el.qtip({
			            content: event.description
			        }); */
			        
				},
				eventClick: function(event, element) {
					if(userData.type!="S"){
					var title = "";
					swal({   title: "Update",   
							 text: "Enter the Subject Name",   
							 //type: "warning",   
							 confirmButtonText: "Update",
							 cancelButtonText :"Cancel",
							 showCancelButton: true, 
							 inputField: true,
							 closeOnConfirm: false,
							 closeOnCancel: false
						 },function(input){
							 	 swal("Updated!", "Timetable has been Updated.", "success");
								 event.title = input;
								 $('#calendar').fullCalendar('updateEvent', event);
								 
							 	});
					
			        // = title;

					}

			    }
			});
			
			
			
			 $('#calendar').fullCalendar( 'addEventSource',        
				        function(start, end, status,callback) {
				            // When requested, dynamically generate virtual
				            // events for every monday and wednesday.
				            var events = [];
				            start = moment("2014-12-01");
				            end = moment("2015-06-01");
				            var dayArray = []; 
				            var day;
				            $.ajaxSetup({
				    			async:false
				    		});
				            $.ajax({
					            url: url,
					            dataType: 'json',
					            data: {
					                sem : sem,
					                fullName : fullName
					            },
					            success: function(doc) {
					                var events = [];
					                days = [];
					               
					                	days = _.groupBy(doc,"day");
					                
					                day = moment().get("day");
					               
					                /* day == 1?dayArray = days.Monday:(day == 2?dayArray = days.Tuesday:(day == 3?dayArray = days.Wednesday:(day == 4?dayArray = days.Thursday:(day == 5?dayArray = days.Friday:(day == 6?dayArray = days.Saturday:([]))))));
					                $.each(dayArray,function(i,field) {
					                    events.push({
					                        title: field.cName,
					                        start: moment().hour(field.from.substring(0,2) == "10"?(10):(field.from.substring(0,2) == "11"?(11):(field.from.substring(0,2) == "12"?(12):(field.from.substring(0,1) == "01"?(13):(field.from.substring(0,2) == "02"?(14):(field.from.substring(0,2) == "03"?(15):(field.from.substring(0,2) == "04"?(16):(field.from.substring(0,2) == "05"?(17):(00))))))))).minute(00).second(00),
					                    	end:moment().hour(field.to.substring(0,2) == "10"?(10):(field.to.substring(0,2) == "11"?(11):(field.to.substring(0,2) == "12"?(12):(field.to.substring(0,2) == "01"?(13):(field.to.substring(0,2) == "02"?(14):(field.to.substring(0,2) == "03"?(15):(field.to.substring(0,2) == "04"?(16):(field.to.substring(0,2) == "05"?(17):(00))))))))).minute(00).second(00)// will be parsed
					                    });
					                }); 
					                events.push({
				                        title: "Lunch Break",
				                        start: moment().hour(13).minute(00).second(00),
				                    	end:moment().hour(14).minute(00).second(00)// will be parsed
				                    });
					                //events = _.groupBy(doc,"day");
					                callback(events); */
					            }
					        });

				            for (loop = start._d.getTime();loop <= end._d.getTime();loop = loop + (24 * 60 * 60 * 1000)) {

				                var test_date = new Date(loop);
								
				                
				                
				                if (test_date.getDay() == 1) {
				                    // we're in Moday, create the event
				                    /* events.push({
				                        title: 'I hate mondays - Garfield',
				                        start: new Date(test_date.setHours(10, 00)),
				                        end: new Date(test_date.setHours(10, 40)),
				                    }); */
				                    if(days.Monday!=undefined){
				                	$.each(days.Monday,function(i,field) {
					                    events.push({
					                        title: field.cName,
					                        description:field.cInfo,
					                        start: moment(loop).hour(field.from.substring(0,2) == "10"?(10):(field.from.substring(0,2) == "11"?(11):(field.from.substring(0,2) == "12"?(12):(field.from.substring(0,1) == "01"?(13):(field.from.substring(0,2) == "02"?(14):(field.from.substring(0,2) == "03"?(15):(field.from.substring(0,2) == "04"?(16):(field.from.substring(0,2) == "05"?(17):(00))))))))).minute(00).second(00),
					                    	end:moment(loop).hour(field.to.substring(0,2) == "10"?(10):(field.to.substring(0,2) == "11"?(11):(field.to.substring(0,2) == "12"?(12):(field.to.substring(0,2) == "01"?(13):(field.to.substring(0,2) == "02"?(14):(field.to.substring(0,2) == "03"?(15):(field.to.substring(0,2) == "04"?(16):(field.to.substring(0,2) == "05"?(17):(00))))))))).minute(00).second(00)// will be parsed
					                    });
					                }); 
				                    }
					                events.push({
				                        title: "Lunch Break",
				                        start: moment(loop).hour(13).minute(00).second(00),
				                    	end:moment(loop).hour(14).minute(00).second(00)// will be parsed
				                    });
				                }

				                else if (test_date.getDay() == 2) {
				                    // we're in Wednesday, create the Tuesday event
				                    if(days.Tuesday!=undefined){
				                	$.each(days.Tuesday,function(i,field) {
					                    events.push({
					                        title: field.cName,
					                        description:field.cInfo,
					                        start: moment(loop).hour(field.from.substring(0,2) == "10"?(10):(field.from.substring(0,2) == "11"?(11):(field.from.substring(0,2) == "12"?(12):(field.from.substring(0,1) == "01"?(13):(field.from.substring(0,2) == "02"?(14):(field.from.substring(0,2) == "03"?(15):(field.from.substring(0,2) == "04"?(16):(field.from.substring(0,2) == "05"?(17):(00))))))))).minute(00).second(00),
					                    	end:moment(loop).hour(field.to.substring(0,2) == "10"?(10):(field.to.substring(0,2) == "11"?(11):(field.to.substring(0,2) == "12"?(12):(field.to.substring(0,2) == "01"?(13):(field.to.substring(0,2) == "02"?(14):(field.to.substring(0,2) == "03"?(15):(field.to.substring(0,2) == "04"?(16):(field.to.substring(0,2) == "05"?(17):(00))))))))).minute(00).second(00)// will be parsed
					                    });
					               	 });
				                    }
					                events.push({
				                        title: "Lunch Break",
				                        start: moment(loop).hour(13).minute(00).second(00),
				                    	end:moment(loop).hour(14).minute(00).second(00)// will be parsed
				                    });
				                }
				                else if (test_date.getDay() == 3) {
				                    // we're in Wednesday, create the Wednesday event
				                    if(days.Wednesday!=undefined){
				                	$.each(days.Wednesday,function(i,field) {
					                    events.push({
					                        title: field.cName,
					                        description:field.cInfo,
					                        start: moment(loop).hour(field.from.substring(0,2) == "10"?(10):(field.from.substring(0,2) == "11"?(11):(field.from.substring(0,2) == "12"?(12):(field.from.substring(0,1) == "01"?(13):(field.from.substring(0,2) == "02"?(14):(field.from.substring(0,2) == "03"?(15):(field.from.substring(0,2) == "04"?(16):(field.from.substring(0,2) == "05"?(17):(00))))))))).minute(00).second(00),
					                    	end:moment(loop).hour(field.to.substring(0,2) == "10"?(10):(field.to.substring(0,2) == "11"?(11):(field.to.substring(0,2) == "12"?(12):(field.to.substring(0,2) == "01"?(13):(field.to.substring(0,2) == "02"?(14):(field.to.substring(0,2) == "03"?(15):(field.to.substring(0,2) == "04"?(16):(field.to.substring(0,2) == "05"?(17):(00))))))))).minute(00).second(00)// will be parsed
					                    });
					                });
				                    }
					                events.push({
				                        title: "Lunch Break",
				                        start: moment(loop).hour(13).minute(00).second(00),
				                    	end:moment(loop).hour(14).minute(00).second(00)// will be parsed
				                    });
				                }
				                else if (test_date.getDay() == 4) {
				                    // we're in Wednesday, create the Thursday event
				                    if(days.Thursday!=undefined){
				                	$.each(days.Thursday,function(i,field) {
					                    events.push({
					                        title: field.cName,
					                        description:field.cInfo,
					                        start: moment(loop).hour(field.from.substring(0,2) == "10"?(10):(field.from.substring(0,2) == "11"?(11):(field.from.substring(0,2) == "12"?(12):(field.from.substring(0,1) == "01"?(13):(field.from.substring(0,2) == "02"?(14):(field.from.substring(0,2) == "03"?(15):(field.from.substring(0,2) == "04"?(16):(field.from.substring(0,2) == "05"?(17):(00))))))))).minute(00).second(00),
					                    	end:moment(loop).hour(field.to.substring(0,2) == "10"?(10):(field.to.substring(0,2) == "11"?(11):(field.to.substring(0,2) == "12"?(12):(field.to.substring(0,2) == "01"?(13):(field.to.substring(0,2) == "02"?(14):(field.to.substring(0,2) == "03"?(15):(field.to.substring(0,2) == "04"?(16):(field.to.substring(0,2) == "05"?(17):(00))))))))).minute(00).second(00)// will be parsed
					                    });
					                });
				                    }
					                events.push({
				                        title: "Lunch Break",
				                        start: moment(loop).hour(13).minute(00).second(00),
				                    	end:moment(loop).hour(14).minute(00).second(00)// will be parsed
				                    });
				                }
				                else if (test_date.getDay() == 5) {
				                    // we're in Friday, create the Wednesday event
				                    if(days.Friday!=undefined){
				                	$.each(days.Friday,function(i,field) {
					                    events.push({
					                        title: field.cName,
					                        description:field.cInfo,
					                        start: moment(loop).hour(field.from.substring(0,2) == "10"?(10):(field.from.substring(0,2) == "11"?(11):(field.from.substring(0,2) == "12"?(12):(field.from.substring(0,1) == "01"?(13):(field.from.substring(0,2) == "02"?(14):(field.from.substring(0,2) == "03"?(15):(field.from.substring(0,2) == "04"?(16):(field.from.substring(0,2) == "05"?(17):(00))))))))).minute(00).second(00),
					                    	end:moment(loop).hour(field.to.substring(0,2) == "10"?(10):(field.to.substring(0,2) == "11"?(11):(field.to.substring(0,2) == "12"?(12):(field.to.substring(0,2) == "01"?(13):(field.to.substring(0,2) == "02"?(14):(field.to.substring(0,2) == "03"?(15):(field.to.substring(0,2) == "04"?(16):(field.to.substring(0,2) == "05"?(17):(00))))))))).minute(00).second(00)// will be parsed
					                    });
					                });
				                    }
					                events.push({
				                        title: "Lunch Break",
				                        start: moment(loop).hour(13).minute(00).second(00),
				                    	end:moment(loop).hour(14).minute(00).second(00)// will be parsed
				                    });
				                }
				                else if (test_date.getDay() == 6) {
				                    // we're in Saturday, create the Wednesday event
				                    if(days.Saturday!=undefined){
				                	$.each(days.Saturday,function(i,field) {
					                    events.push({
					                        title: field.cName,
					                        description:field.cInfo,
					                        start: moment(loop).hour(field.from.substring(0,2) == "10"?(10):(field.from.substring(0,2) == "11"?(11):(field.from.substring(0,2) == "12"?(12):(field.from.substring(0,1) == "01"?(13):(field.from.substring(0,2) == "02"?(14):(field.from.substring(0,2) == "03"?(15):(field.from.substring(0,2) == "04"?(16):(field.from.substring(0,2) == "05"?(17):(00))))))))).minute(00).second(00),
					                    	end:moment(loop).hour(field.to.substring(0,2) == "10"?(10):(field.to.substring(0,2) == "11"?(11):(field.to.substring(0,2) == "12"?(12):(field.to.substring(0,2) == "01"?(13):(field.to.substring(0,2) == "02"?(14):(field.to.substring(0,2) == "03"?(15):(field.to.substring(0,2) == "04"?(16):(field.to.substring(0,2) == "05"?(17):(00))))))))).minute(00).second(00)// will be parsed
					                    });
					                });
				                    }
					                events.push({
				                        title: "Lunch Break",
				                        start: moment(loop).hour(13).minute(00).second(00),
				                    	end:moment(loop).hour(14).minute(00).second(00)// will be parsed
				                    });
				                }
				            } // for loop

				            // return events generated
				            callback( events );
				        }
				    );
		}

		
	});

</script>
<style>

	body {
		margin: 0;
		padding: 0;
		font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
		font-size: 14px;
	}

	#top {
		background: #eee;
		border-bottom: 1px solid #ddd;
		padding: 0 10px;
		line-height: 40px;
		font-size: 12px;
	}
	.left { float: left }
	.right { float: right }
	.clear { clear: both }

	#script-warning, #loading { display: none }
	#script-warning { font-weight: bold; color: red }

	#calendar {
		max-width: 900px;
		margin: 40px auto;
		padding: 0 10px;
	}

	.tzo {
		color: #000;
	}

</style>
</head>
<body>
<nav class="navbar navbar-inverse navbar-static-top" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">CollegeTimeTable</a>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <!-- <li><a href="admin.jsp">Home</a></li> -->
        <li><a href="user.jsp">Home<span class="sr-only">(current)</span></a></li>
        <li class="active"><a href="timeTable.jsp">Timetable</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <!-- <li><a href="#">Link</a></li> -->
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">My Profile<span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">My Account</a></li>
            <li><a href="#">Settings</a></li>
            <li><a href="#">More Info</a></li>
            <li class="divider"></li>
            <li><a href="logout.jsp">Log Out</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>
<%
String emailId = (String)session.getAttribute("logged_id");
session.setAttribute("logged_id",emailId);
System.out.println(emailId);
if(emailId == null){
	session.setAttribute("logget_id",null);
	response.sendRedirect("../login/login.jsp");
}
%>

<div class="container">
	<div class="row">
        <div id='top'>
			<div class='left'>
				Timezone:
				<select id='timezone-selector'>
					<option value='' selected>none</option>
					<option value='local'>local</option>
					<option value='UTC'>UTC</option>
				</select>
			</div>
			<!-- <div class='right'>
				<span id='loading'>loading...</span>
				<span id='script-warning'><code>php/get-events.php</code> must be running.</span>
			</div> -->
			<div class='clear'></div>
		</div>
		<div id='calendar'></div>
	</div>
</div>

</body>
</html>