<%@ page language="java" contentType="application/json; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@	page import="java.sql.*,
				 java.lang.*,
				 databaseconnection.*,
				 java.text.SimpleDateFormat,
				 java.util.*,
				 java.io.*,
				 javax.servlet.*,
				 javax.servlet.http.*"
%>
<%@page import="org.json.simple.JSONObject"%>
<%
String emailId = (String)session.getAttribute("logged_id");
session.setAttribute("logged_id",emailId);
System.out.println(emailId);
if(emailId == null){
	response.sendRedirect("../login/login.jsp");
}

String sem = request.getParameter("sem");
Statement st = null;
PreparedStatement pst = null;
ResultSet rs=null;
try {
	Connection con = databasecon.getconnection();
	pst = con.prepareStatement(" select * from timetable_info where sem='"+sem+"'");
	rs=pst.executeQuery();
	List<JSONObject> timetable = new ArrayList<JSONObject>();
	while (rs.next())
	{	
		JSONObject obj = new JSONObject();
		obj.put("cId",rs.getString(3));
		obj.put("cName",rs.getString(4));
		obj.put("cInfo",rs.getString(5));
		obj.put("lect",rs.getString(6));
		obj.put("day",rs.getString(7));
		obj.put("from",rs.getString(8));
		obj.put("to",rs.getString(9));
		timetable.add(obj);
	}
	out.print(timetable);
 	timetable.clear();
	pst.close();
	con.close();
	response.setContentType("application/json");
} catch (Exception ex) {
	ex.printStackTrace();
	out.println(ex);
}

%>