<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@	page import="java.sql.*,
				 java.lang.*,
				 databaseconnection.*,
				 java.text.SimpleDateFormat,
				 java.util.*,
				 java.io.*,
				 javax.servlet.*,
				 javax.servlet.http.*"
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Admin</title>
<link href="../js/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href='../js/fullcalendar/fullcalendar.css' rel='stylesheet' />
<link href='../js/fullcalendar/fullcalendar.print.css' rel='stylesheet' media='print' />
<link href="../css/sweet-alert.css" rel="stylesheet">
<link href="../js/datatable/css/jquery.dataTables.css" rel="stylesheet">
<link href="../js/datatable/css/dataTables.bootstrap.css" rel="stylesheet">
<script src="../js/jquery-1.9.1.min.js"></script>
<script src="../js/bootstrap/js/bootstrap.min.js"></script>
<script src='../js/fullcalendar/lib/moment.min.js'></script>
<!-- <script src='../lib/jquery.min.js'></script> -->
<script src='../js/fullcalendar/fullcalendar.min.js'></script>
<script src="../js/sweet-alert.js"></script>
<script src="../js/jstorage.js"></script>
<script src="../js/underscore-min.js"></script>
<script src="../js/datatable/js/jquery.dataTables.min.js"></script>
<script src="../js/datatable/js/dataTables.bootstrap.js"></script>

</head>
<body>
 
<nav class="navbar navbar-inverse" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">CollegeTimeTable</a>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active"><a href="admin.jsp">Home</a></li>
        <!-- <li><a href="adminHome.jsp">Admin<span class="sr-only">(current)</span></a></li> -->
        <li><a href="addTimetable.jsp">Timetable<span class="sr-only">(current)</span></a></li>
      </ul>
      <!-- <form class="navbar-form navbar-left" role="search">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
      </form> -->
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">My Profile<span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">My Account</a></li>
            <li><a href="#">Settings</a></li>
            <li><a href="#">More Info</a></li>
            <li class="divider"></li>
            <li><a href="logout.jsp">Log Out</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>
<%
String emailId = (String)session.getAttribute("logged_id");
session.setAttribute("logged_id",emailId);
System.out.println(emailId);
if(emailId == null || !emailId.equals("admin@gmail.com")){
	session.setAttribute("logget_id",null);
	response.sendRedirect("../login/login.jsp");
}

%>
    <!-- Page Content -->
    <div class="container">

        <div class="row">
            <div class="col-lg-12 text-center">
                <h1>WELCOME ADMIN</h1>
            </div>
        </div>
        <!-- /.row -->
		 <div class="row">
			<div class="col-xs-10 col-xs-offset-1">
				<div class="row" id="mst-table"> 
				<table id="userInfoTable" class="table table-condensed table-bordered table-hover">
					<thead>
						<tr>
							<th class="col-xs-1">SeqNo</th>
							<th class="col-xs-4">EmailId</th>
							<th class="col-xs-2">FirstName</th>
							<th class="col-xs-2">LastName</th>
							<th class="col-xs-2">UserId</th>
							<th class="col-xs-1">Status</th>
							<th class="col-xs-1"></th>
						</tr>
					</thead>
				</table>	
			</div>
			</div>
		
		</div>	
    </div>
    <!-- /.container -->

 
<script type="text/javascript">
$(document).ready(function(){
	
	
	$.get( "getUserInfo.jsp",function(userInfoData){
		var emailId = null;
		$("#userInfoTable").dataTable({
			"bPaginate" : true,
			"bDestroy" : true,
			"bProcessing" : true,
			"bAutoWidth" : false,
			"iDisplayLength" : 5,
			'bLengthChange' : false,
			"aoColumns" : [{
					"mData" : "seqNo"
				}, {
					"mData" : "emailId",
					"mRender":function(data,type,full){
						emailId = data;
						return emailId;
					}
				}, {
					"mData" : "firstName"
				}, {
					"mData" : "lastName"
				}, {
					"mData" : "userId"
				}, {
					"mData" : "status",
					"mRender" : function(status,type,full){
						return(status=="A")?"Active":"Inactive";
					}
				},{
					"mData":null,
					"bSortable":false,
   					 "fnRender": function () {
   						 		
		    					return '<div class="btn-group"><button type="button"class="btn btn-default dropdown-toggle"data-toggle="dropdown">'+
		    							 '<i class="fa fa-cog"></i>Actions<span class="caret"></span></button><ul class="dropdown-menu"role="menu">'+
		    								'<li><a class="click" id="'+emailId+'"><i class="fa fa-pencil"></i>Activate</a></li>';
   					 				}
				}
			],
			aaData : userInfoData
		});
		
		$(".click").on("click",function(){
			$.get("activateUser.jsp",{emailId:this.id},function(){
				swal("Activate", "User has been Activated", "success");
				setTimeout(function() {
					window.location.reload();
				}, 3000);
				
			});
		});
	});
	
	
	
});
 
</script>   
</body>
</html>