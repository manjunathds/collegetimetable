<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@	page import="java.sql.*,
				 java.lang.*,
				 databaseconnection.*,
				 java.text.SimpleDateFormat,
				 java.util.*,
				 java.io.*,
				 javax.servlet.*,
				 javax.servlet.http.*"
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SaveTimeTable</title>
</head>
<body>
<%
String emailId = (String)session.getAttribute("logged_id");
session.setAttribute("logged_id",emailId);
System.out.println(emailId);
if(emailId == null){
	response.sendRedirect("../login/login.jsp");
}

List<HashMap<String,String>> timeTable = new ArrayList<HashMap<String,String>>();
for(int i=1;i<=6;i++){
	HashMap<String,String> obj = new HashMap<String,String>();
	obj.put("sem",request.getParameter("semId"));
	obj.put("day",request.getParameter("dayId"));
	obj.put("from",request.getParameter(i+"_f"));
	obj.put("to",request.getParameter(i+"_t"));
	obj.put("lecId",request.getParameter(i+"_lectId"));
	obj.put("cId",request.getParameter(i+"_cid"));
	obj.put("cInfo",request.getParameter(i+"_cinfo"));
	timeTable.add(obj);
}
Statement st = null;
PreparedStatement pst = null;
ResultSet rs=null;
List<String> lect = new ArrayList<String>();
try {
	Connection con = databasecon.getconnection();
	for(HashMap<String,String> each:timeTable){
	pst = con.prepareStatement("insert into timetable_info values(?,?,?,?,?,?,?,?,?)");
	pst.setString(1,null);
	pst.setString(2,each.get("sem"));
	pst.setString(3,each.get("cId"));
	pst.setString(4,each.get("cInfo"));
	pst.setString(5,each.get("cInfo"));
	pst.setString(6,each.get("lecId"));
	pst.setString(7,each.get("day"));
	pst.setString(8,each.get("from"));
	pst.setString(9,each.get("to"));
	pst.executeUpdate();
	}
	pst.close();
	con.close();
	response.sendRedirect("addTimetable.jsp");
} catch (Exception ex) {
	ex.printStackTrace();
	out.println(ex);
}
%>
</body>
</html>