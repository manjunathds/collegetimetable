<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@	page import="java.sql.*,
				 java.lang.*,
				 databaseconnection.*,
				 java.text.SimpleDateFormat,
				 java.util.*,
				 java.io.*,
				 javax.servlet.*,
				 javax.servlet.http.*"
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add Timetable</title>
<link href="../js/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="../js/select2-3.5.2/select2.css" rel="stylesheet">
<link href="../js/select2-3.5.2/select2-bootstrap.css" rel="stylesheet">

<script src="../js/jquery-1.9.1.min.js"></script>
<script src="../js/bootstrap/js/bootstrap.min.js"></script>
<script src="../js/select2-3.5.2/select2.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	
	$("#sem").select2({placeholder:"Sem"});
	$("#day").select2({placeholder:"Day"});
	$("#1_lect").select2({placeholder:"Select Lecturer-Id"});
	$("#2_lect").select2({placeholder:"Select Lecturer-Id"});
	$("#3_lect").select2({placeholder:"Select Lecturer-Id"});
	$("#4_lect").select2({placeholder:"Select Lecturer-Id"});
	$("#5_lect").select2({placeholder:"Select Lecturer-Id"});
	$("#6_lect").select2({placeholder:"Select Lecturer-Id"});
	
	$("#1_lect").change(function(data){
		$("#1_lectId").val($(this).val());
		console.log($(this).val());
	});
	$("#2_lect").on("change",function(data){
		$("#2_lectId").val($(this).val());
	});
	$("#3_lect").on("change",function(data){
		$("#3_lectId").val($(this).val());
	});
	$("#4_lect").on("change",function(data){
		$("#4_lectId").val($(this).val());
	});
	$("#5_lect").on("change",function(data){
		$("#5_lectId").val($(this).val());
	});
	$("#6_lect").on("change",function(data){
		$("#6_lectId").val($(this).val());
	});
	$("#sem").on("change",function(data){
		$("#semId").val($(this).val());
		console.log($(this).val());
	});
	$("#day").on("change",function(data){
		$("#dayId").val($(this).val());
		console.log($(this).val());
	});
	
	function logout(url){
		
	}
});
</script>

</head>
<body>
<%

String emailId = (String)session.getAttribute("logged_id");
session.setAttribute("logged_id",emailId);
System.out.println(emailId);
if(emailId == null || !emailId.equals("admin@gmail.com")){
	session.setAttribute("logged_id",null);
	response.sendRedirect("../login/login.jsp");
}

Statement st = null;
PreparedStatement pst = null;
ResultSet rs=null;
List<String> lect = new ArrayList<String>();
try {
	Connection con = databasecon.getconnection();
	pst = con.prepareStatement(" select * from user_account where type='L' and status='A'");
	rs=pst.executeQuery();
	while (rs.next())
	{
		lect.add(rs.getString("first_name")+""+rs.getString("last_name"));
	}
	
	pst.close();
	con.close();
} catch (Exception ex) {
	ex.printStackTrace();
	out.println(ex);
}
%>
<nav class="navbar navbar-inverse" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">CollegeTimeTable</a>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="admin.jsp">Home</a></li>
        <!-- <li><a href="adminHome.jsp">Admin<span class="sr-only">(current)</span></a></li> -->
        <li class="active"><a href="addTimetable.jsp">Timetable<span class="sr-only">(current)</span></a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">My Profile<span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">My Account</a></li>
            <li><a href="#">Settings</a></li>
            <li><a href="#">More Info</a></li>
            <li class="divider"></li>
            <li><a href="logout.jsp">Log Out</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>
<div class="container">
<form action="saveTimetable.jsp" class="form-horizontal">	
	<div class="row">
          	  <div class="col-xs-2">
             	 <h4>Select Semister</h4>
              </div>
              <div class="col-xs-1">
	              <select class="form-control" id="sem">
	              	<option></option>
	             	<option>1</option>
	             	<option>2</option>
	             	<option>3</option>
	             	<option>4</option>
	             	<option>5</option>
	             	<option>6</option>
	              </select>
	              <input type="hidden" id="semId" name="semId"/>
              </div>
              <div class="col-xs-2">
	              <select class="form-control" id="day">
	              	<option></option>
	             	<option>Monday</option>
	             	<option>Tuesday</option>
	             	<option>Wednesday</option>
	             	<option>Thursday</option>
	             	<option>Friday</option>
	             	<option>Saturday</option>
	              </select>
	              <input type="hidden" id="dayId" name="dayId"/>
              </div>
	</div>
	<br>
	<div class="row">
		<div class="col-xs-12">
			<table id="PODetailsTable" class="table table-condensed table-bordered table-hover col-xs-48">
				<thead>
					<tr style="background-color: #f6bec4; height=35px; valign=middle" >
						<th class="headerstyle">#</th>
						<th class="headerstyle">From</th>
						<th class="headerstyle">To</th>
						<th class="headerstyle">Lecturer-Id</th>
						<th class="headerstyle">Course-Id</th>
						<th class="headerstyle">Course-Info</th>
					</tr>
				</thead>
				<tbody>
							<tr>
								<td class="col-xs-1">
									<span>1</span>
								</td>
								<td class="col-xs-1">
									<input type="text" id="1_f" name="1_f" class="form-control" type="text" value="10 AM" readonly />
								</td>
								<td class="col-xs-1">
									<input type="text" id="1_t" name="1_t" class="form-control" type="text" value="11 AM" readonly />
								</td>
								<td class="col-xs-2">
									<select class="form-control" id="1_lect" name="1_lect">
										<option></option>
										<% for(String each : lect){%>
										<%-- <c:forEach items="${lect}" var="record"> --%>
        									<option>
        									<%= each %>
        									</option>
       									<%-- </c:forEach> --%>
       									<%}%>
									</select>
									<input type="hidden" id="1_lectId" name="1_lectId" /> 
								</td>
							  	<td class="col-xs-2">
							  		<input type="text" id="1_cid" name="1_cid" class="form-control"  maxlength="13" />
								</td>
								
								<td class="col-xs-3">
									<textarea type="text" id="1_cinfo" name="1_cinfo" class="form-control" maxlength="500"></textarea>
								</td>
							</tr>
							<tr>
								<td class="col-xs-1">
									<span>2</span>
								</td>
								<td class="col-xs-1">
									<input type="text" id="2_f" name="2_f" class="form-control" type="text" value="11 AM" readonly  />
								</td>
								<td class="col-xs-1">
									<input type="text" id="2_t" name="2_t" class="form-control" type="text" value="12 PM" readonly  />
								</td>
								<td class="col-xs-2">
									<select class="form-control" id="2_lect" name="2_lect">
										<option></option>
										<% for(String each : lect){%>
										<option>
										<%= each %>
										</option>
										<%}%>
									</select>
									<input type="hidden" id="2_lectId" name="2_lectId" />
								</td>
							  	<td class="col-xs-2">
							  		<input  type="text"  id="2_cid" name="2_cid" class="form-control" maxlength="13" />
								</td>
								
								<td class="col-xs-3">
									<textarea type="text" id="2_cinfo" name="2_cinfo" class="form-control" maxlength="500"></textarea>
								</td>
							</tr>
							<tr>
								<td class="col-xs-1">
									<span>3</span>
								</td>
								<td class="col-xs-1">
									<input type="text" id="3_f" name="3_f" class="form-control" type="text" value="12 PM" readonly  />
								</td>
								<td class="col-xs-1">
									<input type="text" id="3_t" name="3_t" class="form-control" type="text" value="01 PM" readonly  />
								</td>
								<td class="col-xs-2">
									<select class="form-control" id="3_lect" name="3_lect">
										<option></option>
										<% for(String each : lect){%>
										<option>
										<%= each %>
										</option>
										<%}%>
									</select>
									<input type="hidden" id="3_lectId" name="3_lectId" />
								</td>
							  	<td class="col-xs-2">
							  		<input type="text" id="3_cid" name="3_cid"  class="form-control"   maxlength="13" />
								</td>
								
								<td class="col-xs-3">
									<textarea type="text" id="3_cinfo" name="3_cinfo" class="form-control" maxlength="500"></textarea>
								</td>
							</tr>
							<tr>
								<td class="col-xs-1">
									<span>4</span>
								</td>
								<td class="col-xs-1">
									<input type="text" id="4_f" name="4_f" class="form-control" type="text" value="02 PM" readonly  />
								</td>
								<td class="col-xs-1">
									<input type="text" id="4_t" name="4_t" class="form-control" type="text" value="03 PM" readonly  />
								</td>
								<td class="col-xs-2">
									<select class="form-control" id="4_lect" name="4_lect">
										<option></option>
										<% for(String each : lect){%>
										<option>
										<%= each %>
										</option>
										<%}%>
									</select>
									<input type="hidden" id="4_lectId" name="4_lectId" />
								</td>
							  	<td class="col-xs-2">
							  		<input  type="text"  id="4_cid" name="4_cid" class="form-control"   maxlength="13" />
								</td>
								
								<td class="col-xs-3">
									<textarea type="text" id="4_cinfo" name="4_cinfo" class="form-control" maxlength="500"></textarea>
								</td>
							</tr>
							<tr>
								<td class="col-xs-1">
									<span>5</span>
								</td>
								<td class="col-xs-1">
									<input type="text" id="5_f" name="5_f" class="form-control" type="text" value="03 PM" readonly  />
								</td>
								<td class="col-xs-1">
									<input type="text" id="5_t" name="5_t" class="form-control" type="text" value="04 PM" readonly  />
								</td>
								<td class="col-xs-2">
									<select class="form-control" id="5_lect" name="5_lect">
										<option></option>
										<% for(String each : lect){%>
										<option>
										<%= each %>
										</option>
										<%}%>
									</select>
									<input type="hidden" id="5_lectId" name="5_lectId" />
								</td>
							  	<td class="col-xs-2">
							  		<input type="text" id="5_cid" name="5_cid" class="form-control"   maxlength="13" />
								</td>
								
								<td class="col-xs-3">
									<textarea type="text" id="5_cinfo" name="5_cinfo" class="form-control" maxlength="500"></textarea>
								</td>
							</tr>
							<tr>
								<td class="col-xs-1">
									<span>6</span>
								</td>
								<td class="col-xs-1">
									<input type="text" id="6_f" name="6_f" class="form-control" type="text" value="04 PM" readonly  />
								</td>
								<td class="col-xs-1">
									<input type="text" id="6_t" name="6_t" class="form-control" type="text" value="05 PM" readonly  />
								</td>
								<td class="col-xs-2">
									<select class="form-control" id="6_lect" name="6_lect">
										<option></option>
										<% for(String each : lect){%>
										<option>
										<%= each %>
										</option>
										<%}%>
									</select>
									<input type="hidden" id="6_lectId" name="6_lectId" />
								</td>
							  	<td class="col-xs-2">
							  		<input  type="text" id="6_cid" name="6_cid" class="form-control"   maxlength="13" />
								</td>
								
								<td class="col-xs-3">
									<textarea type="text" id="6_cinfo" name="6_cinfo" class="form-control" maxlength="500"></textarea>
								</td>
							</tr>
				</tbody>
			</table>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-xs-offset-9 col-xs-1">
			<button type="submit" class="btn btn-success"> SAVE </button>
		</div>
		<div class="col-xs-1">
			<a  class="btn btn-warning" href="admin.jsp">CANCEL</a>
		</div>
	</div>
</form>
</div>
</body>
</html>